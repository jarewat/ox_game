import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author jarewat
 */
public class OXGame {

    public static void main(String[] args) {
        int round;
        int row = -1;
        int col = -1;
        String table[][] = new String[3][3];
        String player = null;
        showWelcome();
        setEmptyTable(table);
        for (int i = 1; i < 9; i++) {
            round = i;
            showTable(table);
            player = switchPlayer(i);
            showTurn(player);
            if (input(table, player, row, col)) {
                break;
            }
            checkDraw(round);
        }
    }

    static public void showWelcome() {
        System.out.println("----- Welcome to OX GAME -----");
    }

    static public void showTable(String[][] table) {
        System.out.println("_ 1 2 3 _");
        for (int i = 0; i < 3; i++) {
            System.out.print((i + 1) + " ");
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.print("|");
            System.out.println("");
        }
        System.out.println("¯ ¯ ¯ ¯ ¯");
    }

    static public void showTurn(String player) {
        if (player.equals("X")) {
            System.out.println("Turn  Player 2 (X)");
        } else {
            System.out.println("Turn  Player 1 (O)");
        }
    }

    static public String switchPlayer(int n) {
        if ((n % 2) != 0) {
            return "O";
        } else {
            return "X";
        }
    }

    static public void setEmptyTable(String[][] table) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j] = "-";
            }
        }
    }

    static public boolean input(String table[][], String player, int row, int col) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please enter Row and Col :");

        try {
            row = sc.nextInt();
            col = sc.nextInt();
            if (checkEmpty(table, player, row, col)) {
                table[row - 1][col - 1] = player;
            }
            if (checkWin(table, player, row, col)) {
                return true;
            }

        } catch (InputMismatchException e) {
            System.out.println("Input is wrong try input number");
            input(table, player, row, col);
        }catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("row or col is wrong try again");
            input(table, player, row, col);
        }

        return false;
    }



    static public boolean checkEmpty(String[][] table, String player, int row, int col) {
        if (!table[row - 1][col - 1].equals("-")) {
            System.out.println("Is not empty please try again");
            input(table, player, row, col);
        } else {
            return true;
        }
        return false;
    }

    static public boolean checkWin(String table[][], String player, int row, int col) {
        if (checkCol(table, player, col)) {
            showTable(table);
            showWin(player);
            return true;
        } else if (checkRow(table, player, row)) {
            showTable(table);
            showWin(player);
            return true;
        } else if (checkX1(table, player)) {
            showTable(table);
            showWin(player);
            return true;
        } else if (checkX2(table, player)) {
            showTable(table);
            showWin(player);
            return true;
        }
        return false;
    }

    static public boolean checkCol(String table[][], String player, int col) {
        if (table[0][col - 1].equals(player)) {
            if (table[1][col - 1].equals(player)) {
                if (table[2][col - 1].equals(player)) {
                    return true;
                }
            }
        }
        return false;
    }

    static public boolean checkRow(String table[][], String player, int row) {
        if (table[row - 1][0].equals(player)) {
            if (table[row - 1][1].equals(player)) {
                if (table[row - 1][2].equals(player)) {
                    return true;
                }
            }
        }
        return false;
    }

    static public boolean checkX1(String table[][], String player) {
        if (table[2][0].equals(player)) {
            if (table[1][1].equals(player)) {
                if (table[0][2].equals(player)) {
                    return true;
                }
            }
        }
        return false;
    }

    static public boolean checkX2(String table[][], String player) {
        if (table[0][0].equals(player)) {
            if (table[1][1].equals(player)) {
                if (table[2][2].equals(player)) {
                    return true;
                }
            }
        }
        return false;
    }

    static public void showWin(String player) {
        if (player.equals("X")) {
            System.out.println("!!! Winer : Player 2 (X) !!!");
            showBye();
        } else {
            System.out.println("!!! Winer : Player 1 (O) !!!");
            showBye();
        }

    }

    static public void checkDraw(int round) {
        if (round == 9) {
            System.out.println("!!! Draw !!!");
            showBye();
        }
    }

    static public void showBye() {
        System.out.println("----- ! Bye ! -----");
    }
}